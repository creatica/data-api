import { Entity, PrimaryColumn, Column, OneToMany } from 'typeorm'
import Reservation from './reservation'

@Entity()
export default class User {
  @PrimaryColumn()
  ID: string
  @Column()
  type: 'teacher' | 'student'
  @Column()
  name: string
  @Column({ unique: true })
  email: string
  @Column()
  password: string
  @OneToMany(type => Reservation, reservation => reservation.user)
  reservations: Reservation[]
}
