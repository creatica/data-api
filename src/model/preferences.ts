import {
  Entity,
  Column,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn
} from 'typeorm'
import User from './user'

@Entity()
export default class Preferences {
  @PrimaryGeneratedColumn()
  ID: number
  @Column({default: 'project'})
  type: 'project' | 'stilte' | 'prive'
  @Column({ nullable: true })
  amount?: number
  @Column({ nullable: true })
  building?: 'H' | 'WD' | 'WN'
  @Column({ nullable: true })
  floor?: number
  @Column({ nullable: true })
  duration?: number
  @OneToOne(type => User)
  @JoinColumn({ name: 'userID' })
  user: User
}
