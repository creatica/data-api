import { Entity, Column, PrimaryColumn, ManyToOne, JoinColumn } from "typeorm";
import User from "./user";
import Classroom from "./classroom";

@Entity()
export default class Reservation {
  @Column()
  dateTime: Date
  @Column()
  plannedDuration: number
  @Column()
  endDateTime: Date
  @ManyToOne(type => User, user => user.reservations, { primary: true })
  user: User
  @ManyToOne(type => Classroom, classroom => classroom.reservations, { primary: true })
  classroom: Classroom
}
