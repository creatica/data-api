import { Entity, PrimaryColumn, Column, ManyToOne, OneToMany } from 'typeorm'
import Reservation from './reservation';

@Entity()
export default class Classroom {
  @PrimaryColumn()
  ID: string
  @Column()
  capacity: number
  @Column()
  peoplePresent: number
  @Column()
  building: string
  @Column()
  floor: string
  @Column()
  type: 'project' | 'stilte' | 'prive'
  @OneToMany(type => Reservation, reservation => reservation.classroom)
  reservations: Reservation[]
}
