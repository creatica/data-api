import 'reflect-metadata'
import * as express from 'express'
import { createConnection } from 'typeorm'
import User from './model/user'
import Reservation from './model/reservation'
import Classroom from './model/classroom'
import bodyParser = require('body-parser')
import * as dotenv from 'dotenv'
import {
  isRegisterRequestBody,
  isLoginRequestBody,
  isClassroomFilterRequestBody,
  isClassroomPostRequestBody
} from './requestModel'
import * as cors from 'cors'
import Preferences from './model/preferences'

const dotenvObject = dotenv.config()

if (
  dotenvObject.error ||
  !process.env.DBHOST ||
  !process.env.DBUSER ||
  !process.env.DATABASE
) {
  console.log(`.env file required with at least HOST, USER and DATABASE values`)
  process.exit(0)
}

const app: express.Application = express()
const port: number = parseInt(process.env.PORT) || 5000

createConnection({
  type: 'mysql',
  host: process.env.DBHOST,
  port: 3306,
  username: process.env.DBUSER,
  password: process.env.DBPASS,
  database: process.env.DATABASE,
  entities: [User, Classroom, Reservation, Preferences],
  synchronize: true
})
  .then(connection => {
    const users = connection.getRepository(User)
    const reservations = connection.getRepository(Reservation)
    const classrooms = connection.getRepository(Classroom)
    const preferences = connection.getRepository(Preferences)

    const jsonParser = bodyParser.json()

    app.use(cors())

    app.get('/', (req, res) => {
      res.status(404)
      res.send('404 not found')
    })

    app.post('/user', jsonParser, async (req, res) => {
      if (!isRegisterRequestBody(req.body)) {
        const message = `${Date.now} POST /user: wrong request body IP:${
          req.ip
        }`
        console.log(message)
        res.status(400)
        res.send('Bad request')
      }

      users
        .findOne(req.body)
        .then(alreadyRegistered => {
          if (alreadyRegistered) {
            res.status(422)
            res.send()
          }
          const newUser = users.create(req.body)
          users
            .save(newUser)
            .then(savedUser => {
              res.status(201)
              res.send()
            })
            .catch(error => {
              const message = `${
                Date.now
              } POST /user/login: error adding user to database: ${error}`
              console.log(message)
              res.status(500)
              res.send()
            })
        })
        .catch(error => {
          const message = `${
            Date.now
          } POST /user/login: error while checking if user already exists: ${error}`
          console.log(message)
        })
    })

    app.post('/user/login', jsonParser, async (req, res) => {
      if (!isLoginRequestBody(req.body)) {
        const message = `${Date.now} POST /user/login: wrong request body IP:${
          req.ip
        }`
        console.log(message)
        res.status(400)
        res.send('Bad request')
      }

      users
        .findOne(req.body)
        .then(alreadyRegistered => {
          if (!alreadyRegistered) {
            res.status(404)
            res.send()
          }
          res.status(200)
          res.send(alreadyRegistered)
        })
        .catch(findError => {
          const message = `${
            Date.now
          } POST /user/login: error searching for users in database: ${findError}`
          console.log(message)
          res.status(500)
          res.send()
        })
    })

    app.post('/classroom/filter', jsonParser, (req, res) => {
      if (!isClassroomFilterRequestBody(req.body)) {
        const message = `${
          Date.now
        } POST /classroom/filter: wrong request body IP:${req.ip}`
        console.log(message)
        res.status(400)
        res.send()
      }
      if (!req.header('userID')) {
        res.status(401)
        res.send()
      }
      users.findOne(req.header('userID')).then(user => {
        const newPreference = {
          user: user,
          ...req.body
        }
        const newPreferences = preferences.create(newPreference)
        preferences.save(newPreferences).catch(error => {
          const message = `${
            Date.now
          } POST /classroom/filter: can't update user's preferences:${error}`
          console.log(message)
        })
      }).catch(error => {
        const message = `${
          Date.now
        } POST /classroom/filter: can't get user:${error}`
        console.log(message)
      })
      // TODO: als je een prive lokaal zoekt, zoek je naar een leeg lokaal. Als die gevonden is en aangeklikt, dan onstaat er dus eigenlijk een reservering.
      classrooms
        .find(req.body)
        .then(rooms => {
          res.status(200)
          res.send(
            rooms.filter(
              room => room.capacity - room.peoplePresent >= req.body.amount
            )
          )
        })
        .catch(findError => {
          const message = `${
            Date.now
          } POST /user/login: error searching for users in database: ${findError}`
          console.log(message)
          res.status(500)
          res.send()
        })
    })

    app.post('/classroom', jsonParser, (req, res) => {
      if (!isClassroomPostRequestBody(req.body)) {
        const message = `${Date.now} POST /user: wrong request body IP:${
          req.ip
        }`
        console.log(message)
        res.status(400)
        res.send()
      }
      if (!req.header('userID')) {
        res.status(401)
        res.send()
      }
      const classroom = classrooms.create(req.body)
      classrooms
        .save(classroom)
        .then(value => {
          res.status(201)
          res.send(value)
        })
        .catch(error => {
          const message = `${
            Date.now
          } POST /user/login: error inserting classroom in database: ${error}`
          console.log(message)
          res.status(500)
          res.send()
        })
    })

    app.get('/preferences')

    app.listen(port, () => {
      console.log(`Listening at port: ${port}`)
    })
  })
  .catch(reason => {
    console.log(`Error establishing connection to the database: ${reason}`)
    process.exit(0)
  })
