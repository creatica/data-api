export interface LoginRequestBody {
  email: string
  password: string
}

export function isLoginRequestBody(arg: any): arg is RegisterRequestBody {
  const converted = <RegisterRequestBody>arg
  return (
    converted.email !== undefined &&
    typeof converted.email == 'string' &&
    converted.password !== undefined &&
    typeof converted.password == 'string'
  )
}

export interface RegisterRequestBody {
  ID: string
  type: 'teacher' | 'student'
  name: string
  email: string
  password: string
}

export function isRegisterRequestBody(arg: any): arg is RegisterRequestBody {
  const converted = <RegisterRequestBody>arg
  return (
    converted.ID !== undefined &&
    typeof converted.ID == 'string' &&
    converted.type !== undefined &&
    typeof converted.type == 'string' &&
    converted.name !== undefined &&
    typeof converted.name == 'string' &&
    isLoginRequestBody(arg)
  )
}

export type ClassroomFilterRequestBody =
  | {
      type: 'project' | 'stilte'
      amount?: number
      building?: 'H' | 'WD' | 'WN'
      floor?: number
    }
  | {
      type: 'prive'
      amount?: number
      building?: string
      floor?: number
      duration: number
    }

export function isClassroomFilterRequestBody(
  arg: any
): arg is ClassroomFilterRequestBody {
  const converted = <ClassroomFilterRequestBody>arg
  return (
    (converted.type != undefined &&
      typeof converted.type == 'string' &&
      (converted.type == 'project' || converted.type == 'stilte')) ||
    (converted.type !== undefined &&
      typeof converted.type == 'string' &&
      converted.type === 'prive' &&
      converted.duration != undefined)
  )
}

export interface ClassroomPostRequestBody {
  ID: string
  capacity: number
  peoplePresent: number
  building: string
  floor: number
  type: 'project' | 'stilte' | 'prive'
}

export function isClassroomPostRequestBody(
  arg: any
): arg is ClassroomPostRequestBody {
  const converted = <ClassroomPostRequestBody>arg
  return (
    converted.ID != undefined &&
    typeof converted.ID === 'string' &&
    converted.capacity != undefined &&
    typeof converted.capacity === 'number' &&
    converted.peoplePresent != undefined &&
    typeof converted.peoplePresent === 'number' &&
    converted.building != undefined &&
    typeof converted.building === 'number' &&
    converted.floor != undefined &&
    typeof converted.floor === 'string' &&
    converted.type != undefined &&
    typeof converted.type === 'string' &&
    (converted.type === 'project' ||
      converted.type === 'stilte' ||
      converted.type === 'prive')
  )
}
